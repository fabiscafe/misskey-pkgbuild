# Misskey PKGBUILD

Arch Linux PKGBUILD for [Misskey](https://github.com/misskey-dev/misskey)

## Misskey Wiki
[This is your link](https://misskey-hub.net/en/)

# How To set it up
## 1. Install Misskey
As all the dependencies are in the official repo, you only need to clone the repo and make it

    git clone https://gitlab.com/fabiscafe/misskey-pkgbuild.git
    cd misskey-pkgbuild
    makepkg -cirs

## 2. Change Misskey settings
By default Misskey is pretty much preconfigured in the reverse-proxy way. The only setting you need to change is the server url in */etc/webapps/misskey/default.conf* to reflect your server setup from a public (internet-facing) point. No `localhost` or anything internal.

## 3. PopstgreSQL setup
Follow the [Arch Wiki](https://wiki.archlinux.org/title/PostgreSQL) to set it up and startz up postgresql.

After thats done you need to create a user and a database. By default this is set to:

user: **misskey**,<br>
database: **misskey**

If you want to change that, make sure to also edit your *default.conf* from step 1.

    sudo -iu postgres
    createuser --interactive
        Enter name of role to add: misskey
        Shall the new role be a superuser? (y/n) n
        Shall the new role be allowed to create databases? (y/n) y
        Shall the new role be allowed to create more new roles? (y/n) n
    
    createdb misskey -O misskey
    exit

## 4. Redis
[Redis is usable by default](https://wiki.archlinux.org/title/Redis#Installation). Just start the service

    # systemctl start redis.service

## 5. Initialize Misskey
Note: After this you should not change any setting anymore.
To initialize Misskey, switch to the right path and init Misskey as *misskey* system user.

    cd "/usr/share/webapps/misskey"
    # runuser -u misskey -- env HOME="/usr/share/webapps/misskey" yarn run init

## 6. Start Misskey
The last thing to to is to start the service

    # systemctl start misskey.service

After this Misskey is available on http://\<server-ip\>:3000

## 7. Setup a reverse proxy
You can use [Nginx](https://wiki.archlinux.org/title/Nginx) for this. The reverse proxy needs to point to *http://localhost:3000*
